![](example.jpg)

# ColoredQR

New type of qr code, which uses 4 colors to code information.

### Parameters

Mode             | Usage                            | Description
----             | -----                            | -----------
`-h` \| `--help` | `-h` \| `--help`                 | Show help message and exit.
`--encode`       | `--encode [image] [text] [size]` | Draws new CQR code to `[image]` with size `[size]`px.
`--decode`       | `--decode [image]`               | Find, normalize and recognize CQR code from `[image]`.
`--decodeNorm`   | `--decodeNorm [image]`           | Just recognize CQR code from `[image]`.
`--realTime`     | `--realTime`                     | Find, normalize and recognize CQR code from web cam in Real Time.

Examples:
- toolsCQR.py --encode example.jpg "textExample" 300
- toolsCQR.py --decodeNorm example.jpg