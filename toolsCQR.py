import cv2
import numpy
import argparse

def GetCRC16CCITT(data):
    crc16ret = 0xFFFF
    
    for b in data:
        crc16ret ^= b << 8
        crc16ret &= 0xFFFF
        for i in range(0,8):
            if (crc16ret & 0x8000):
                crc16ret=(crc16ret << 1) ^ 0x1021
            else :
                crc16ret=crc16ret << 1
            crc16ret &=0xFFFF

    return crc16ret.to_bytes(2, "big")

def EncodeRawCQR(data):
    def byteTo4(b):
        b1 = b // 64; b %= 64
        b2 = b // 16; b %= 16
        b3 = b // 4;  b %= 4
        
        return numpy.array([b1, b2, b3, b], numpy.uint8)
    
    #Позиционная метка.
    rect = numpy.array([[1, 1, 1, 1, 1, 1, 1],
                        [1, 0, 0, 0, 0, 0, 1],
                        [1, 0, 1, 1, 1, 0, 1],
                        [1, 0, 1, 1, 1, 0, 1],
                        [1, 0, 1, 1, 1, 0, 1],
                        [1, 0, 0, 0, 0, 0, 1],
                        [1, 1, 1, 1, 1, 1, 1]], numpy.uint8)
    
    assert(len(data) <= 238)
    
    #len + data + crc.
    #Переводим в систему счисления с основанием 4.
    rawData = numpy.hstack(numpy.array([byteTo4(b) for b in (bytes([len(data)]) + data + GetCRC16CCITT(data))], numpy.uint8))
    
    #Размер.
    size = max([17, int(numpy.ceil(numpy.sqrt(rawData.shape[0] + 192)))])
    
    #Заполнение до нужного размера.
    rawData = numpy.hstack((rawData, numpy.random.randint(0, 4, size = (size ** 2 - rawData.shape[0] - 192), dtype = numpy.uint8)))
    
    #Пустой массив
    rawCQR = numpy.zeros((size, size), numpy.uint8)
    
    #Добавляем позиционные метки.
    rawCQR[:7, :7]        = rect
    rawCQR[:7, size - 7:] = rect * 2
    rawCQR[size - 7:, :7] = rect * 3
    
    #Заполняем.
    rawCQR[ :8, 8:-8] = rawData[:8 * (size - 16)].reshape(8, size - 16)
    rawCQR[8:-8]      = rawData[ 8 * (size - 16):-8 * (size - 8)].reshape(size - 16, size)
    rawCQR[-8:, 8:]   = rawData[-8 * (size - 8):].reshape(8, size - 8)
    
    return rawCQR
    
def DecodeRawCQR(rawCQR):
    #Вырезаем данные
    ld = numpy.hstack((numpy.hstack(rawCQR[:8, 8:-8]), numpy.hstack(rawCQR[8:-8]), numpy.hstack(rawCQR[-8:, 8:])))
    
    #Переводим в десятиричную.
    rawBytes = [ld[b - 3] * 64 + ld[b - 2] * 16 + ld[b - 1] * 4 + ld[b] for b in range(3, len(ld), 4)]
    
    hash = bytes(rawBytes[(rawBytes[0] + 1):(rawBytes[0] + 3)])
    data = bytes(rawBytes[1:rawBytes[0] + 1])
    
    assert(GetCRC16CCITT(data) == hash)

    return bytes(data)


def VisualizeCQR(data, size, colors = [(255, 255, 255), (255, 0, 0), (0, 255, 0), (0, 0, 255)]):
    return cv2.resize(numpy.array(list(map(lambda arr: list(map(lambda v: colors[v], arr)), EncodeRawCQR(data))), numpy.uint8), (size, size), interpolation = cv2.INTER_AREA)

def ReadImgCQR(cqrImg, isNorm = False, pts = None):
    #Пример цвета с позиционной метки.
    def getAvgColor(img):
        mask = numpy.array([[[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]],
                            [[1, 1, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [1, 1, 1]],
                            [[1, 1, 1], [0, 0, 0], [1, 1, 1], [1, 1, 1], [1, 1, 1], [0, 0, 0], [1, 1, 1]],
                            [[1, 1, 1], [0, 0, 0], [1, 1, 1], [1, 1, 1], [1, 1, 1], [0, 0, 0], [1, 1, 1]],
                            [[1, 1, 1], [0, 0, 0], [1, 1, 1], [1, 1, 1], [1, 1, 1], [0, 0, 0], [1, 1, 1]],
                            [[1, 1, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [1, 1, 1]],
                            [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]]], numpy.uint8)
        
        return((numpy.mean(img * mask, axis = (0, 1)) * 49 // 33).astype(numpy.uint8))
    
    #Рассчитываем цвета для масок.
    def getMaskColors(img):
        def compMask(clr, hueDiff = 20, satDiff = 100, valDiff = 100):
            low  = [clr[0] - hueDiff, max([0, clr[1] - satDiff]),   max([0, clr[2] - valDiff])]
            high = [clr[0] + hueDiff, min([255, clr[1] + satDiff]), min([255, clr[2] + valDiff])]
            
            if(low[0] < 0):
                low_2  = [low[0] + 180, low[1],   low[2]]
                high_2 = [180,          high[1], high[2]]
                
                low[0] = 0
                
                return numpy.array([low, high, low_2, high_2]) 
                
            if(high[0] > 180):
                low_2  = [0,              low[1],  low[2]]
                high_2 = [high[0] - 180, high[1], high[2]]
                
                high[0] = 180
                
                return numpy.array([low, high, low_2, high_2]) 
                
            return numpy.array([low, high])
                 
        return (compMask(getAvgColor(img[:7, :7])), compMask(getAvgColor(img[:7, img.shape[0] - 7:])), compMask(getAvgColor(img[img.shape[0] - 7:, :7])))
    
    def getMask(qr_hsv, maskColor):
        return (cv2.inRange(qr_hsv, maskColor[0], maskColor[1]) // 255) if (maskColor.shape[0] != 4) else (cv2.bitwise_or(cv2.inRange(qr_hsv, maskColor[0], maskColor[1]), 
                                                                                                                          cv2.inRange(qr_hsv, maskColor[2], maskColor[3])) // 255)
    
    def getBytes(qr_hsv):
        maskColors = getMaskColors(qr_hsv)
        
        qrRaw = numpy.zeros(qr_hsv.shape[:2], dtype = numpy.uint8)
        
        qrRaw += getMask(qr_hsv, maskColors[0])
        qrRaw += getMask(qr_hsv, maskColors[1]) * 2
        qrRaw += getMask(qr_hsv, maskColors[2]) * 3
        
        try:
            return DecodeRawCQR(qrRaw)
        except:
            return None
    
    if(not isNorm):
        if(pts is None):
            pts = FindCQR(cqrImg)
        
        assert(not pts is None)
        
        cqrImg = NormalizePerspective(cqrImg, pts)
        
        isNorm = True
    
    assert(isNorm)
    
    for size in range(17, 35):
        msg = getBytes(cv2.cvtColor(cv2.resize(cqrImg, (size, size), interpolation = cv2.INTER_AREA), cv2.COLOR_BGR2HSV))
        
        if(msg): return msg

def NormalizePerspective(image, rect):
    (tl, tr, br, bl) = rect
    
    # Sizes
    widthA = numpy.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = numpy.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
   
    heightA = numpy.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = numpy.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    
    # Couse cqr is square.
    size = int(max(widthA, widthB, heightA, heightB))
    
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = numpy.array([
        [0, 0],
        [size - 1, 0],
        [size - 1, size - 1],
        [0, size - 1]], dtype = numpy.float32)
 
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    
    # return the warped image
    return cv2.warpPerspective(image, M, (size, size))

def FindCQR(rawImg, qrDecoder = cv2.QRCodeDetector()):
    isFind, qrContour = qrDecoder.detect(rawImg)
    
    return qrContour.reshape(4, 2) if isFind else None

def ReadRealTime(keyToExit = 27):
    camStream = cv2.VideoCapture(0)
    
    qrDecoder = cv2.QRCodeDetector()
    
    while cv2.waitKey(16) != keyToExit: 
        _, frame =  camStream.read()
        
        qrContour = FindCQR(frame, qrDecoder)
        
        if(qrContour is None):
            cv2.imshow("Live", frame); continue
        
        msg = "Decoded: " + str(ReadImgCQR(frame, False, qrContour))
        
        pts = qrContour.astype(int)
        
        cv2.line(frame, tuple(pts[0]), tuple(pts[1]),(0,0,255), 5)
        cv2.line(frame, tuple(pts[1]), tuple(pts[2]),(0,0,255), 5)
        cv2.line(frame, tuple(pts[2]), tuple(pts[3]),(0,0,255), 5)
        cv2.line(frame, tuple(pts[3]), tuple(pts[0]),(0,0,255), 5)
        
        cv2.putText(frame, msg, tuple(pts[1] + 10), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255), 2, cv2.LINE_AA)
   
        cv2.imshow("Live", frame)

          
if __name__ == "__main__":
    argParser = argparse.ArgumentParser("New type of cqr code, which uses 4 colors to code information.")
    
    argParser.add_argument("--encode",     dest = "toEncode",       metavar = "",    help = "usage: --encode image.jpg 'textExample' 300 <<-- Makes a new CQR code, size is 300px", nargs = 3,)
    argParser.add_argument("--decode",     dest = "toDecode",       metavar = "",    help = "usage: --decode image.jpg <<-- Find, normalize and recognize CQR code.")
    argParser.add_argument("--decodeNorm", dest = "toDecodeNorm",   metavar = "",    help = "usage: --decodeNorm image.jpg <<-- Just recognize CQR code.")
    argParser.add_argument("--realTime",   dest = "realTime",                        help = "usage: --realTime <<-- Find, normalize and recognize CQR code from web cam in Real Time.", action = "store_true")
    
    argvStore = argParser.parse_args()
    
    if(argvStore.toEncode):
        cv2.imwrite("%s" % argvStore.toEncode[0], VisualizeCQR(argvStore.toEncode[1].encode(), int(argvStore.toEncode[2])))
        
    elif(argvStore.toDecode):
        print(ReadImgCQR(cv2.imread("%s" % argvStore.toDecode)))
        
    elif(argvStore.toDecodeNorm):
        print(ReadImgCQR(cv2.imread("%s" % argvStore.toDecodeNorm), True))
    
    elif(argvStore.realTime):
        ReadRealTime()
    
    else:
        argParser.print_help()
    